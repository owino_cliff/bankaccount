# Bank Account REST API 

## What is the essence of this project?
The goal of this mini project is to write a simple micro web service to mimic a “Bank Account”. Through this web service, one can query about the balance, deposit money, and withdraw money. Just like any Bank, there are restrictions on how many transactions/amounts it can handle. The details are described below.

- Write a simple “Bank Account” web service using REST API design principles.
- Program should have 3 REST API endpoints: Balance, Deposit, and Withdrawal
- No requirement for authentication assume the web service is for one account only and is open to the world
- No requirement for the backend store you can store it in a file or database (your decision)
- Balance endpoint this will return the outstanding balance

- Deposit endpoint credits the account with the specified amount
  - Max deposit for the day = $150K
  -  Max deposit per transaction = $40K
  -  Max deposit frequency = 4 transactions/day

- Withdrawal endpoint deducts the account with the specified amount
  - Max withdrawal for the day = $50K
  - Max withdrawal per transaction = $20K
  - Max withdrawal frequency = 3 transactions/day
  - Cannot withdraw when balance is less than withdrawal amount

- The service should handle all the error cases and return the appropriate error HTTP status code and error message (Eg. If an attempt is to withdraw greater than $20k in a single transaction, the error message should say “Exceeded Maximum Withdrawal Per Transaction”).

###### Project execution requirements? ###
 This is a source repository meaning you have to build locally.
 1. Ensure you have Java installed (minimum Java 8)
 2. Ensure you have MySQL installed
 3. Ensure you have Git installed
 ```
 4. git clone https://owino_cliff@bitbucket.org/owino_cliff/bankaccount.git
 ```
 5. Navigate into the folder
 6. open the terminal and execute the following commands

```
      - javac -d bin -sourcepath src account\src\main\java\com\bank\account\BankAccount.java (compilation)
```
```
      - java -cp bin; com.bank.account (running)
```
###### REST Endpoints

  1. http://localhost:8080/bank/account/balance/{accountNumber} 
      -  HTTP Verb ->  GET 
      -  Path param {accountNumber} - The use account number being queried.

  2. http://localhost:8080/bank/account/deposit
      -  HTTP Verb -> POST
      -  Request Header  ->  "Content-Type: application/json"
      -  Request Body    
 ```
     {
        "accountNumber": ":number",
        "description": ":String",
        "amount": ":number"
     }
 ```

   3. http://localhost:8080/bank/account/withdraw
      -  HTTP Verb -> POST
      -  Request Header  ->  "Content-Type: application/json"
      -  Request Body    
 ```
     {
        "accountNumber": ":number",
        "description": ":String",
        "amount": ":number"
     }
 ```
