package com.bank.account.controllers;

import com.bank.account.entities.Account;
import com.bank.account.enums.TransactionType;
import com.bank.account.exception.TransactionException;
import com.bank.account.models.Response;
import com.bank.account.models.TransactionRequest;
import com.bank.account.services.AccountRepository;
import com.bank.account.services.TransactionService;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Optional;
import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.ArgumentMatchers.eq;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.core.env.Environment;

/**
 *
 * @author User
 */
public class TransactionServiceTest {

    @Mock
    private AccountRepository accountRepository;

    @Spy
    @InjectMocks
    private TransactionService transactionService;

    @Mock
    private Environment environment;

    public TransactionServiceTest() {
    }

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        when(environment.getProperty(eq("withdrawal.max.amount"),
                eq(Double.class))).thenReturn(20.0);
        when(environment.getProperty(eq("withdrawal.max.daily.frequency"),
                eq(Integer.class))).thenReturn(2);
        when(environment.getProperty(eq("withdrawal.max.daily.amount"),
                eq(Double.class))).thenReturn(50.0);

        when(environment.getProperty(eq("deposit.max.amount"),
                eq(Double.class))).thenReturn(40.0);
        when(environment.getProperty(eq("deposit.max.daily.frequency"),
                eq(Integer.class))).thenReturn(4);
        when(environment.getProperty(eq("deposit.max.daily.amount"),
                eq(Double.class))).thenReturn(150.0);

    }

    @Test(expected = TransactionException.class)
    public void transactAccountBalanceUnknownAccount() throws TransactionException {
        TransactionRequest transactionRequest = new TransactionRequest();
        Mockito.when(accountRepository.findByAccountNumber(Mockito.eq(transactionRequest.getAccountNumber())))
                .thenReturn(Optional.<Account>empty());

        transactionService.transact(transactionRequest, TransactionType.BALANCE_ENQUIRY);
    }

    @Test
    public void transactAccountBalanceCorrect() throws TransactionException {
        TransactionRequest request = new TransactionRequest();
        request.setAccountNumber("123456");
        Mockito.when(accountRepository.findByAccountNumber(Mockito.eq(request.getAccountNumber())))
                .thenReturn(Optional.<Account>of(new Account(
                                        //Integer id, String accountNumber, String accountName, BigDecimal balance, Date createdAt, Date updatedAt
                                        null,
                                        "123456",
                                        "Isaac Ruto",
                                        new BigDecimal("0.00"),
                                        new Timestamp(0),
                                        new Timestamp(0)
                                )));

        Response response = transactionService.transact(request, TransactionType.BALANCE_ENQUIRY);
        assertNotNull(response);
    }

}
