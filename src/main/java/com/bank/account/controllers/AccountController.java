package com.bank.account.controllers;

import com.bank.account.enums.ApiResponseStatus;
import com.bank.account.enums.TransactionType;
import com.bank.account.exception.TransactionException;
import com.bank.account.models.Response;
import com.bank.account.models.TransactionRequest;
import com.bank.account.services.TransactionService;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Clifford.Owino
 */
@RestController
@RequestMapping("/account")
public class AccountController {

    private final Logger log = LoggerFactory.getLogger(AccountController.class);

    @Autowired
    TransactionService transactionService;

    @RequestMapping(value = "/balance/{accountNumber}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> list(@PathVariable("accountNumber") String accountNumber, HttpServletRequest request) {
        ResponseEntity responseEntity;
        try {
            TransactionRequest request1 = new TransactionRequest();
            request1.setAccountNumber(accountNumber);
            Response response = transactionService.transact(request1, TransactionType.BALANCE_ENQUIRY);
            responseEntity = new ResponseEntity(response, HttpStatus.OK);
        } catch (TransactionException ex) {
            log.error("Balance Enquiry process on account number {} failed due to {}.", accountNumber, ex.getMessage());
            responseEntity = new ResponseEntity(new Response(ApiResponseStatus.ERROR.getResponseStatus(), ex.getMessage(), null), HttpStatus.BAD_REQUEST);
        }
        return responseEntity;
    }

    @RequestMapping(value = "/deposit", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deposit(@RequestBody TransactionRequest transactionRequest, HttpServletRequest request) {

        ResponseEntity responseEntity;
        try {
            Response response = transactionService.transact(transactionRequest, TransactionType.FUNDS_DEPOSIT);
            responseEntity = new ResponseEntity(response, HttpStatus.OK);
        } catch (TransactionException ex) {
            log.error("Balance Enquiry process on account number {} failed due to {}", transactionRequest.getAccountNumber(), ex.getMessage());
            responseEntity = new ResponseEntity(new Response(ApiResponseStatus.ERROR.getResponseStatus(), ex.getMessage(), null), HttpStatus.BAD_REQUEST);
        }
        return responseEntity;

    }

    @RequestMapping(value = "/withdraw", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> withdraw(@RequestBody TransactionRequest transactionRequest, HttpServletRequest request) {

        ResponseEntity responseEntity;
        try {
            Response response = transactionService.transact(transactionRequest, TransactionType.FUNDS_WITHDRAWAL);
            responseEntity = new ResponseEntity(response, HttpStatus.OK);
        } catch (TransactionException ex) {
            log.error("Balance Enquiry process on account number {} failed due to {}", transactionRequest.getAccountNumber(), ex.getMessage());
            responseEntity = new ResponseEntity(new Response(ApiResponseStatus.ERROR.getResponseStatus(), ex.getMessage(), null), HttpStatus.BAD_REQUEST);
        }
        return responseEntity;

    }

}
