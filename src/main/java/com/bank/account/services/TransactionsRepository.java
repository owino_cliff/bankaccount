package com.bank.account.services;

import com.bank.account.entities.Account;
import com.bank.account.entities.Transactions;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author User
 */
public interface TransactionsRepository extends CrudRepository<Transactions, Integer> {

    public Optional<Transactions> findDistinctByAccountId(Account account);

    @Query(value = "SELECT a FROM Transactions a WHERE a.accountId=:account "
            + "AND a.createdAt >= :startDate "
            + "AND a.createdAt <= :endDate ORDER BY a.createdAt")
    public List<Transactions> searchTransactions(Account account, Timestamp startDate, Timestamp endDate);

}
