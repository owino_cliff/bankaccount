package com.bank.account.services;

import com.bank.account.entities.Account;
import com.bank.account.entities.Transactions;
import com.bank.account.enums.ApiResponseStatus;
import com.bank.account.enums.TransactionType;
import com.bank.account.exception.TransactionException;
import com.bank.account.models.Response;
import com.bank.account.models.TransactionRequest;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import org.joda.time.DateTime;
import java.util.Optional;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author User
 */
@Component
@Transactional
public class TransactionService {

    private final Logger log = LoggerFactory.getLogger(TransactionService.class);

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private TransactionsRepository transactionsRepository;

    @Autowired
    private Environment environment;

    public Response transact(TransactionRequest transactionRequest, TransactionType transactionType) throws TransactionException {

        log.info("Transactions type {} on account number {} initaited", transactionType, transactionRequest.getAccountNumber());
        Response response = null;
        String status;
        String description;

        try {
            Optional<Account> account = accountRepository.findByAccountNumber(transactionRequest.getAccountNumber());

            if (account.isPresent()) {

                switch (transactionType) {
                    case BALANCE_ENQUIRY:
                        response = new Response(ApiResponseStatus.SUCCESS.getResponseStatus(), "Transaction successfull", account.get());
                        break;
                    case FUNDS_DEPOSIT:
                        if (new BigDecimal(environment.getProperty("deposit.max.amount", Double.class)).compareTo(transactionRequest.getAmount()) < 0) {
                            status = ApiResponseStatus.TRANSACTION_LIMIT_BREACH.getResponseStatus();

                            throw new TransactionException(String.format("Amount more than maximum deposit per transaction of $ %,.2f",
                                    environment.getProperty("deposit.max.amount", Double.class)));
                        }

                        Timestamp startDate = new Timestamp(DateTime.now()
                                .withTimeAtStartOfDay()
                                .getMillis());
                        Timestamp endDate = new Timestamp(DateTime.now()
                                .withTimeAtStartOfDay()
                                .plusDays(1)
                                .minusSeconds(1)
                                .getMillis());
                        List<Transactions> transactions = transactionsRepository.searchTransactions(account.get(), startDate, endDate);

                        //■ Max deposit frequency  transactions/day  
                        if (transactions.size() > environment.getProperty("deposit.max.daily.frequency", Integer.class)) {
                            status = ApiResponseStatus.DAILY_TRNX_COUNT_LIMIT_BREACH.getResponseStatus();
                            description = "Daily deposit count limit reached";
                            throw new TransactionException(description);
                        }

                        BigDecimal withdrawnToday = new BigDecimal("0");
                        transactions.stream().forEach((trnx) -> {
                            withdrawnToday.add(trnx.getDr());
                        });

                        if (new BigDecimal(environment.getProperty("deposit.max.daily.amount", Double.class)).compareTo(withdrawnToday.add(transactionRequest.getAmount())) < 0) {
                            status = ApiResponseStatus.DAILY_MAX_WITHDRAW_LIMIT_BREACH.getResponseStatus();
                            description = "Daily deposit amount limit reached";
                            throw new TransactionException(description);
                        }

                        try {
                            Transactions trxn = new Transactions();
                            trxn.setAccountId(account.get());
                            trxn.setCreatedAt(new Timestamp(System.currentTimeMillis()));
                            trxn.setDescription(transactionRequest.getDescription());
                            trxn.setUpdatedAt(new Timestamp(System.currentTimeMillis()));
                            trxn.setDr(transactionRequest.getAmount());
                            trxn.setCr(new BigDecimal(0));
                            transactionRequest.setCreatedAt(trxn.getCreatedAt());
                            transactionsRepository.save(trxn);
                            updateBalance(account.get(), true, transactionRequest.getAmount());
                        } catch (Exception e) {
                            throw e;
                        }
                        status = ApiResponseStatus.SUCCESS.getResponseStatus();
                        description = "Transaction processed successfully";
                        response = new Response(status, description, transactionRequest);
                        break;
                    case FUNDS_WITHDRAWAL:
                        //■ Cannot withdraw when balance is less than withdrawal amount 
                        if (account.get().getBalance().compareTo(transactionRequest.getAmount()) < 0) {
                            status = ApiResponseStatus.INSUFFICIENT_BALANCE.getResponseStatus();
                            description = "Insufficient balance for this transaction";

                            throw new TransactionException(description);
                        }

                        //■ Max withdrawal per transaction = $20K 
                        if (new BigDecimal(environment.getProperty("withdrawal.max.amount", Double.class)).compareTo(transactionRequest.getAmount()) < 0) {
                            status = ApiResponseStatus.TRANSACTION_LIMIT_BREACH.getResponseStatus();
                            description = "Amount more than maximum withdrawable per transaction of ".concat(String.valueOf(environment.getProperty("withdrawal.max.amount", Double.class)));
                            throw new TransactionException(description);
                        }

                        Timestamp startDate1 = new Timestamp(DateTime.now()
                                .withTimeAtStartOfDay()
                                .getMillis());
                        Timestamp endDate1 = new Timestamp(DateTime.now()
                                .withTimeAtStartOfDay()
                                .plusDays(1)
                                .minusSeconds(1)
                                .getMillis());
                        List<Transactions> transactionsLst = transactionsRepository.searchTransactions(account.get(), startDate1, endDate1);

                        //■ Max withdrawal frequency = 3 transactions/day  
                        if (transactionsLst.size() > environment.getProperty("withdrawal.max.daily.frequency", Integer.class)) {
                            status = ApiResponseStatus.DAILY_TRNX_COUNT_LIMIT_BREACH.getResponseStatus();
                            description = "Daily transactions count limit reached";
                            throw new TransactionException(description);
                        }

                        BigDecimal withdrawnToday1 = new BigDecimal("0");
                        transactionsLst.stream().forEach((trnx) -> {
                            withdrawnToday1.add(trnx.getDr());
                        });

                        //■ Max withdrawal for the day = $50K
                        if (new BigDecimal(environment.getProperty("withdrawal.max.daily.amount", Double.class)).compareTo(withdrawnToday1.add(transactionRequest.getAmount())) < 0) {
                            status = ApiResponseStatus.DAILY_MAX_WITHDRAW_LIMIT_BREACH.getResponseStatus();
                            description = "Daily transactions amount limit reached";
                            throw new TransactionException(description);
                        }

                        Transactions trxn1 = new Transactions();
                        trxn1.setAccountId(account.get());
                        trxn1.setCreatedAt(new Timestamp(System.currentTimeMillis()));
                        trxn1.setDescription(transactionRequest.getDescription());
                        trxn1.setUpdatedAt(new Timestamp(System.currentTimeMillis()));
                        trxn1.setCr(transactionRequest.getAmount());
                        trxn1.setDr(new BigDecimal(0));
                        transactionRequest.setCreatedAt(trxn1.getCreatedAt());
                        transactionsRepository.save(trxn1);
                        updateBalance(account.get(), false, transactionRequest.getAmount());

                        status = ApiResponseStatus.SUCCESS.getResponseStatus();
                        description = "Transaction processed successfully";
                        response = new Response(status, description, transactionRequest);
                        break;
                }

                return response;
            }
            log.warn("Account Number {} does not exits.", transactionRequest.getAccountNumber());
            status = ApiResponseStatus.INVALID_ACCOUNT.getResponseStatus();
            description = "Invalid account number provided";
            throw new TransactionException(description);
        } catch (HibernateException | TransactionException e) {
            log.error("{}", e.getMessage());
            throw new TransactionException(e.getMessage());
        }
    }

    private void updateBalance(Account account, boolean isDeposit, BigDecimal amount) {
        if (isDeposit) {
            account.setBalance(account.getBalance().add(amount));
        } else {
            account.setBalance(account.getBalance().subtract(amount));
        }
        accountRepository.save(account);
    }

}
