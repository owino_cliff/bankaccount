package com.bank.account.services;

import com.bank.account.entities.Account;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author User
 */
@Repository
public interface AccountRepository extends CrudRepository<Account, Integer> {
    
    public Optional<Account> findByAccountNumber(String accountNumber);
     
}
