package com.bank.account;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EntityScan(basePackages = "com.bank.account.entities")
@ComponentScan("com.bank.account.*")
public class BankApplication {


    public static void main(String[] args) {
        SpringApplication.run(BankApplication.class, args);
    }

    

}
