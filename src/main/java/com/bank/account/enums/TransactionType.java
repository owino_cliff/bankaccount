/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bank.account.enums;

/**
 *
 * @author User
 */
public enum TransactionType {
    
    BALANCE_ENQUIRY,
    
    FUNDS_DEPOSIT,
    
    FUNDS_WITHDRAWAL;
}
