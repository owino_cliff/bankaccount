package com.bank.account.enums;

/**
 *
 * @author User
 */
public enum ApiResponseStatus {

    SUCCESS("00"),
    ERROR("01"),
    INVALID_ACCOUNT("02"),
    INSUFFICIENT_BALANCE("03"),
    DAILY_TRNX_COUNT_LIMIT_BREACH("04"),
    DAILY_MAX_WITHDRAW_LIMIT_BREACH("05"),
    TRANSACTION_LIMIT_BREACH("06");

    private final String responseStatus;

    private ApiResponseStatus(String responseStatus) {
        this.responseStatus = responseStatus;
    }

    public String getResponseStatus() {
        return responseStatus;
    }

}
