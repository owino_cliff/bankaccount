package com.bank.account.exception;

/**
 *
 * @author User
 */
public class TransactionException extends Exception {

    public TransactionException() {
    }

    public TransactionException(String msg) {
        super(msg);
    }

    public TransactionException(String message, Throwable cause) {
        super(message, cause);
    }

    
}
