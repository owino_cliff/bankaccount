/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bank.account.models;

/**
 *
 * @author User
 * @param <T>
 */
public class Response<T> {
    private String status;
    private String description;
    private T response;

    public Response(String status, String description, T response) {
        this.status = status;
        this.description = description;
        this.response = response;
    }
    
    

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public T getResponse() {
        return response;
    }

    public void setResponse(T response) {
        this.response = response;
    }

    
}
