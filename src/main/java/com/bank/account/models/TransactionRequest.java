package com.bank.account.models;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author User
 */
public class TransactionRequest {

    private String accountNumber;
    private String description;
    private Date createdAt;
    private BigDecimal amount;

    

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }


    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    

}
